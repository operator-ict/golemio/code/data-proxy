# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.3.2] - 2025-03-11

### Removed

-   Remove sentry ([core#128](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/128))

## [1.3.1] - 2024-12-04

### Changed

-   Update Node.js to v20.18.0 ([core#119](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/119))

## [1.3.0] - 2024-11-21

### Changed

-   Azure Blob Storage managed identity env vars update ([infra#304](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/304))

## [1.2.9] - 2024-09-24

-   No changelog

## [1.2.8] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.2.7] - 2024-07-04

### Changed

-   Upgrade to node@20 and express@4.19.2

## [1.2.6] - 2024-03-06

### Fixed

-   Fix the issue with the request aborted by secondary target ([pid#18](https://gitlab.com/operator-ict/golemio/code/data-proxy/-/issues/18))

## [1.2.5] - 2024-01-22

-   No changelog

## [1.2.4] - 2023-07-31

### Changed

-   NodeJS 18.17.0

## [1.2.3] - 2023-07-17

### Fixed

-   Open telemetry initialization

## [1.2.2] - 2023-04-26

-   No changelog

## [1.2.1] - 2023-03-22

-   No changelog

## [1.2.0] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.1] - 2023-02-20

### Changed

-   Proper HTTP proxy implementation using the `http-proxy` package ([data-proxy#15](https://gitlab.com/operator-ict/golemio/code/data-proxy/-/issues/15))

## [1.1.0] - 2023-01-23

### Changed

-   Docker image optimization
-   Migrate to npm

## [1.0.7] - 2022-12-13

### Added

-   Remote debugging using process signals ([data-proxy#12](https://gitlab.com/operator-ict/golemio/code/data-proxy/-/issues/12))
    -   SIGUSR1 - activate inspector (default `ws://127.0.0.1:9229`)
    -   SIGUSR2 - deactivate inspector

## [1.0.6] - 2022-11-29

### Changed

-   Update TypeScript to v4.7.2

## [1.0.5] - 2022-10-04

### Changed

-   Change storage provider from S3 to Azure blob storage ([data-proxy#10](https://gitlab.com/operator-ict/golemio/code/data-proxy/-/issues/10))

## [1.0.4] - 2022-09-01

### Added

-   Add UDP data proxy server ([pid#165](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/165))

## [1.0.2] - 2022-06-30

### Added

-   Add S3 storage service
-   Add proxy stats storage ([data-proxy#4](https://gitlab.com/operator-ict/golemio/code/data-proxy/-/issues/4))

## [1.0.2] - 2022-03-31

### Changed

-   Improve server/client disconnection handling ([data-proxy#2](https://gitlab.com/operator-ict/golemio/code/data-proxy/-/issues/2))

