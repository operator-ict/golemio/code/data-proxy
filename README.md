[![pipeline status](https://gitlab.com/operator-ict/golemio/code/data-proxy/badges/development/pipeline.svg)](https://gitlab.com/operator-ict/golemio/code/data-proxy/commits/development)
[![coverage report](https://gitlab.com/operator-ict/golemio/code/data-proxy/badges/development/coverage.svg)](https://gitlab.com/operator-ict/golemio/code/data-proxy/commits/development)

<!-- TODO replace development with master in the previous links -->

# Golemio Data Platform TCP/HTTP Data Proxy

## Local Installation

### Prerequisites

-   Node.js (https://nodejs.org)

### Installation

Install all dependencies using command:

```
npm install
```

in the application's root directory.

### Build & Run

#### Production

To compile TypeScript code into js (production build):

```bash
npm run build-minimal
# Or npm run build to generate source map files
```

To run the app:

```
npm run start
```

#### Dev/debug

Run via TypeScript (in this case it is not needed to build separately, application will watch for changes and restart on save):

```
npm run dev-start
```

or run with a debugger:

```
npm run dev-start-debug
```

Running the application in any way will load all config variables from environment variables or the .env file. To run, set all environment variables from the `.env.template` file, or copy the `.env.template` file into new `.env` file in root directory and set variables there.

Project uses `dotenv` package: https://www.npmjs.com/package/dotenv

The specific configuration files are in the `config/` directory. For more information see [config files](https://gitlab.com/operator-ict/golemio/code/modules/core/-/blob/development/docs/configuration_files.md#data-proxy)
