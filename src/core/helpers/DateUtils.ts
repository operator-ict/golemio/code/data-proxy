import { DateTime } from "@golemio/core/dist/shared/luxon";

export class DateUtils {
    public static getISODateTime(date = new Date()) {
        return DateTime.fromJSDate(date).setZone("Europe/Prague").toISO();
    }
}
