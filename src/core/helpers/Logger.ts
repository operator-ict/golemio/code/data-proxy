import { createLogger, createRequestLogger } from "@golemio/core/dist/helpers";
import { config } from "../config";

const logger = createLogger({
    projectName: "data-proxy",
    nodeEnv: config.nodeEnv,
    logLevel: config.logLevel,
});

const requestLogger = createRequestLogger(config.nodeEnv, logger);

export { logger as log, requestLogger };
