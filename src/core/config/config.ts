import { ConfigLoader } from "@golemio/core/dist/helpers";
import { IConfiguration, IDataProxyServerDefinition } from ".";

export const config: IConfiguration = {
    logLevel: process.env.LOG_LEVEL,
    nodeEnv: process.env.NODE_ENV || "development",
    appVersion: process.env.npm_package_version,
    port: process.env.PORT,
    inspector: {
        host: process.env.INSPECTOR_HOST,
    },
    dataProxyServerDefinitions: new ConfigLoader("dataProxyServerDefinitions", true).conf as IDataProxyServerDefinition[],
    secondaryTargetReconnectMaxAttempts: Number(process.env.SECONDARY_TARGET_RECONNECT_MAX_ATTEMPTS) || 10,
    secondaryTargetReconnectTimeout: Number(process.env.SECONDARY_TARGET_RECONNECT_TIMEOUT) || 2000,
    statsStorageCron: process.env.STATS_STORAGE_CRON ?? "*/1 * * * *",
    httpProxyRequestTimeoutInMs: process.env.HTTP_PROXY_REQUEST_TIMEOUT_IN_MS
        ? +process.env.HTTP_PROXY_REQUEST_TIMEOUT_IN_MS
        : 30000,
    httpProxyResponseTimeoutInMs: process.env.HTTP_PROXY_RESPONSE_TIMEOUT_IN_MS
        ? +process.env.HTTP_PROXY_RESPONSE_TIMEOUT_IN_MS
        : 30000,
    tcpSocketTimeoutInMs: process.env.TCP_SOCKET_TIMEOUT_IN_MS ? +process.env.TCP_SOCKET_TIMEOUT_IN_MS : 60000,
    storage: {
        enabled: process.env.STORAGE_ENABLED === "true",
        provider: {
            azure: {
                clientId: process.env.AZURE_BLOB_CLIENT_ID ?? "",
                tenantId: process.env.AZURE_BLOB_TENANT_ID ?? "",
                clientSecret: process.env.AZURE_BLOB_CLIENT_SECRET ?? "",
                account: process.env.AZURE_BLOB_ACCOUNT ?? "",
                containerName: process.env.AZURE_BLOB_STORAGE_CONTAINER_NAME ?? "",
                uploadTimeoutInSeconds: Number.parseInt(process.env.AZURE_BLOB_UPLOAD_TIMEOUT_IN_SECONDS ?? "120"),
            },
        },
    },
};
