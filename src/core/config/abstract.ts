import { IStorageConfig } from "@golemio/core/dist/helpers/data-access/storage";

export interface IConfiguration {
    logLevel: string | undefined;
    nodeEnv: string;
    appVersion: string | undefined;
    port: string | undefined;
    inspector: {
        host: string | undefined;
    };
    dataProxyServerDefinitions: IDataProxyServerDefinition[];
    secondaryTargetReconnectMaxAttempts: number;
    secondaryTargetReconnectTimeout: number;
    statsStorageCron: string;
    tcpSocketTimeoutInMs: number;
    httpProxyRequestTimeoutInMs: number;
    httpProxyResponseTimeoutInMs: number;
    storage: IStorageConfig;
}

export enum DataProxyServerType {
    "TCP" = "tcp",
    "UDP" = "udp",
    "HTTP" = "http",
}

export interface ITCPTargetDefinition {
    id: string;
    host: string;
    port: number;
    saveStats?: boolean;
}

export interface IUDPTargetDefinition extends ITCPTargetDefinition {}

export interface IHTTPTargetDefinition {
    id: string;
    url: string;
    headers?: { [key: string]: string };
    saveStats?: boolean;
}
export interface IDataProxyServerDefinition {
    id: string;
    name: string;
    type: DataProxyServerType;
    port: number;
    targets: ITCPTargetDefinition[] | IHTTPTargetDefinition[];
}

export interface ITCPDataProxyServerDefinition extends IDataProxyServerDefinition {
    type: DataProxyServerType.TCP;
    targets: Array<{
        id: string;
        host: string;
        port: number;
    }>;
}

export interface IUDPDataProxyServerDefinition extends IDataProxyServerDefinition {
    type: DataProxyServerType.UDP;
    targets: Array<{
        id: string;
        host: string;
        port: number;
    }>;
}

export interface IHTTPDataProxyServerDefinition extends IDataProxyServerDefinition {
    type: DataProxyServerType.HTTP;
    targets: Array<{ id: string; url: string; headers?: { [key: string]: string } }>;
}
