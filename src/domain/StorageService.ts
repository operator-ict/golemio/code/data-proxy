import { StorageServiceFactory, StorageProvider, AbstractStorageService } from "@golemio/core/dist/helpers/data-access/storage";
import { config } from "../core/config";
import { log } from "../core/helpers";

export class StorageService {
    private static _instance: AbstractStorageService | undefined;

    public static getInstance() {
        if (!this._instance) {
            this._instance = new StorageServiceFactory(config.storage, log).getService(StorageProvider.Azure);
        }

        return this._instance;
    }
}
