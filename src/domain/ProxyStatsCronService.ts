import { Cron } from "croner";
import { config } from "../core/config";
import { log } from "../core/helpers";
import { StorageService } from "./StorageService";
import { IProxyStatsStorage } from "./abstract";

class ProxyStatsCronService {
    private storageObjects: Set<IProxyStatsStorage>;
    private cronJob: Cron;

    constructor() {
        this.storageObjects = new Set();
        this.cronJob = Cron(config.statsStorageCron, this.collectAndSaveStats(), {
            timezone: "Europe/Prague",
        });
    }

    public deactivate() {
        this.cronJob.stop();
    }

    public addStorage(storage: IProxyStatsStorage) {
        this.storageObjects.add(storage);
    }

    private collectAndSaveStats() {
        return async () => {
            const storageService = StorageService.getInstance();
            for (const storage of this.storageObjects) {
                if (!storage.hasStatsChanged()) continue;

                const data = JSON.stringify(storage.statsBuffer, null, 2);
                const pathPrefix = storage.proxyServerId;
                storage.resetStatsBuffer();

                log.info("[ProxyStatsCronService] Saving stats for " + storage.proxyServerId);
                log.debug(data);
                await storageService?.uploadFile(data, pathPrefix, "json");
            }
        };
    }
}

export const proxyStatsCronService = new ProxyStatsCronService();
