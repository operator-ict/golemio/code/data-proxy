export interface IProxyStatsStorage {
    proxyServerId: string;
    statsBuffer: IStatsBuffer;

    resetStatsBuffer: () => void;
    hasStatsChanged: () => boolean;
    markRequest: (targetServerId: string, params: IProxyStatsRequestParams, shouldBufferRequest: boolean) => void;
}

export interface IProxyStatsRequestParams {
    status: RequestResultStatus;
    clientId: string;
    connectionEstablished?: string;
    connectionDuration?: number;
    serverHost?: string;
    serverPort?: number;
    serverUrl?: string;
    serverRequestMethod?: string;
    reconnectAttempts?: number;
    error?: string;
}

export interface IStatsBuffer {
    statsCreated: string;
    statsUpdated: string;
    requests: Record<string, IProxyStatsRequestParams[]>;
}

export enum RequestResultStatus {
    Initialized = "initialized",
    Rejected = "rejected",
    TimedOut = "timed-out",
    Fulfilled = "fulfilled",
}
