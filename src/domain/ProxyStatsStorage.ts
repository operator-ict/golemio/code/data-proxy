import { DateUtils, log } from "../core/helpers";
import { IProxyStatsStorage, IStatsBuffer, IProxyStatsRequestParams } from "./abstract";
import { proxyStatsCronService } from "./ProxyStatsCronService";

export class ProxyStatsStorage implements IProxyStatsStorage {
    public proxyServerId: string;
    public statsBuffer!: IStatsBuffer;
    private requestHolder: Map<string, IProxyStatsRequestParams>;

    constructor(proxyServerId: string) {
        this.proxyServerId = proxyServerId;
        this.requestHolder = new Map();

        this.resetStatsBuffer();
        proxyStatsCronService.addStorage(this);
    }

    public resetStatsBuffer() {
        const localISO = DateUtils.getISODateTime();
        this.statsBuffer = {
            statsCreated: localISO,
            statsUpdated: localISO,
            requests: {},
        };

        if (this.requestHolder.size > 1000) {
            log.warn(`[ProxyStatsStorage] Request holder size for ${this.proxyServerId} is ${this.requestHolder.size}`);
            this.requestHolder.clear();
        }
    }

    public hasStatsChanged() {
        return this.statsBuffer.statsCreated !== this.statsBuffer.statsUpdated;
    }

    public markRequest(targetServerId: string, params: IProxyStatsRequestParams, shouldBufferRequest = false) {
        const requestKey = targetServerId + "-" + params.clientId;
        let request = this.requestHolder.get(requestKey);

        if (shouldBufferRequest && !request) {
            return;
        }

        this.requestHolder.set(requestKey, {
            ...request,
            ...params,
        });

        if (shouldBufferRequest) {
            this.bufferRequest(targetServerId, params.clientId);
        }
    }

    private bufferRequest(targetServerId: string, clientId: string) {
        if (!this.statsBuffer.requests[targetServerId]) {
            this.statsBuffer.requests[targetServerId] = [];
        }

        const requestKey = targetServerId + "-" + clientId;
        let request = this.requestHolder.get(requestKey)!;

        this.statsBuffer.requests[targetServerId].push(request);
        this.statsBuffer.statsUpdated = DateUtils.getISODateTime();
        this.requestHolder.delete(requestKey);
    }
}
