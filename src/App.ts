import { CustomError, ErrorHandler, HTTPErrorHandler, ICustomErrorObject } from "@golemio/core/dist/shared/golemio-errors";
import express from "@golemio/core/dist/shared/express";
import fs from "fs";
import path from "path";
import http from "http";
import { Server } from "net";
import { Socket as DatagramSocket } from "dgram";
import { createLightship, Lightship } from "@golemio/core/dist/shared/lightship";
import { getServiceHealth, IServiceCheck } from "@golemio/core/dist/helpers";
import {
    config,
    DataProxyServerType,
    IDataProxyServerDefinition,
    IHTTPDataProxyServerDefinition,
    ITCPDataProxyServerDefinition,
    IUDPDataProxyServerDefinition,
} from "./core/config";
import { log, requestLogger } from "./core/helpers";
import { proxyStatsCronService } from "./domain";
import { BaseDataProxyServer, HTTPDataProxyServer, TCPDataProxyServer, UDPDataProxyServer } from "./proxy";

export default class App {
    /// Create a new express application instance
    public express: express.Application = express();
    public server?: http.Server;
    /// The port the express app will listen on
    public port: number = parseInt(config.port || "3010", 10);
    /// The SHA of the last commit from the application running
    private commitSHA: string | undefined = undefined;
    private lightship: Lightship;
    private dataProxyServers: Array<HTTPDataProxyServer | TCPDataProxyServer | UDPDataProxyServer> = [];

    /**
     * Runs configuration methods on the Express instance
     * and start other necessary services (crons, database, middlewares).
     */
    constructor() {
        this.lightship = createLightship({ shutdownHandlerTimeout: 10000 });
        process.on("uncaughtException", (err: Error) => {
            log.warn("data-proxy uncaughtException");
            log.error(err);
            this.lightship.shutdown();
        });
        process.on("unhandledRejection", (err: Error) => {
            log.warn("data-proxy unhandledRejection");
            log.error(err);
            this.lightship.shutdown();
        });
    }

    /**
     * Starts the application
     */
    public start = async (): Promise<void> => {
        try {
            this.commitSHA = await this.loadCommitSHA();
            log.info(`Commit SHA: ${this.commitSHA}`);

            await this.expressServer();

            this.dataProxyServers = this.initDataProxyServers(config.dataProxyServerDefinitions);
            await this.startDataProxyServers(this.dataProxyServers);

            log.info("Started!");
            this.lightship.registerShutdownHandler(async () => {
                await this.gracefulShutdown();
            });
            this.lightship.signalReady();
        } catch (err: any) {
            ErrorHandler.handle(err, log, "error");
        }
    };

    /**
     * Graceful shutdown - terminate connections and server
     */
    private gracefulShutdown = async (): Promise<void> => {
        log.info("Graceful shutdown initiated.");

        await new Promise((done) => this.server?.close(done));
        for (const { server } of this.dataProxyServers) {
            await new Promise((done) => server.close(done));
        }

        proxyStatsCronService.deactivate();
    };

    /**
     * Loading the Commit SHA of the current build
     */
    private loadCommitSHA = async (): Promise<string> => {
        return new Promise<string>((resolve) => {
            fs.readFile(path.join(__dirname, "..", "commitsha"), (err, data) => {
                if (err) {
                    return resolve(undefined as any);
                }
                return resolve(data.toString());
            });
        });
    };

    /**
     * Starts the express server
     */
    private async expressServer(): Promise<void> {
        this.middleware();
        await this.routes();

        this.server = http.createServer(this.express);
        // Setup error handler hook on server error
        this.server.on("error", (err: any) => {
            ErrorHandler.handle(new CustomError("Could not start a server", false, undefined, 1, err), log, "error");
        });
        // Serve the application at the given port
        this.server.listen(this.port, () => {
            // Success callback
            log.info(`Status HTTP server listening at http://localhost:${this.port}/`);
        });
    }

    /**
     * Checking app health
     */
    private healthCheck = async () => {
        const description = {
            app: "Golemio Data Platform TCP/HTTP Data Proxy",
            commitSha: this.commitSHA,
            version: config.appVersion,
        };

        const services: IServiceCheck[] = [];

        const serviceStats = await getServiceHealth(services);

        return { ...description, ...serviceStats };
    };

    /**
     * Using express middlewares
     */
    private middleware = (): void => {
        this.express.use(requestLogger);
    };

    /**
     * Defines express server routes
     */
    private routes = async (): Promise<void> => {
        const defaultRouter = express.Router();

        // base url route handler
        defaultRouter.get(
            ["/", "/healthcheck", "/status"],
            async (req: express.Request, res: express.Response, next: express.NextFunction) => {
                try {
                    const healthStats = await this.healthCheck();
                    if (healthStats.health) {
                        return res.json(healthStats);
                    } else {
                        return res.status(503).send(healthStats);
                    }
                } catch (err) {
                    return res.status(503);
                }
            }
        );

        this.express.use("/", defaultRouter);

        // Not found error - no route was matched
        this.express.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
            next(new CustomError("Not found", true, undefined, 404));
        });

        // Error handler to catch all errors sent by routers (propagated through next(err))
        this.express.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
            const warnCodes = [400, 401, 403, 404, 409, 413, 422];
            const errObject: ICustomErrorObject = HTTPErrorHandler.handle(
                err,
                log,
                warnCodes.includes(err.code) ? "warn" : "error"
            );
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    };

    private initDataProxyServers = (
        definitions: IDataProxyServerDefinition[]
    ): Array<HTTPDataProxyServer | TCPDataProxyServer | UDPDataProxyServer> => {
        this.dataProxyServers = [];
        for (const definition of definitions) {
            let d;
            switch (definition.type) {
                case DataProxyServerType.TCP:
                    d = definition as ITCPDataProxyServerDefinition;
                    this.dataProxyServers.push(new TCPDataProxyServer(d.id, d.name, d.port, d.targets));
                    break;
                case DataProxyServerType.UDP:
                    d = definition as IUDPDataProxyServerDefinition;
                    this.dataProxyServers.push(new UDPDataProxyServer(d.id, d.name, d.port, d.targets));
                    break;
                case DataProxyServerType.HTTP:
                    d = definition as IHTTPDataProxyServerDefinition;
                    this.dataProxyServers.push(new HTTPDataProxyServer(d.id, d.name, d.port, d.targets));
                    break;
                default:
                    log.warn(`Undefined server definition type. ${definition.name} ${definition.type}`);
            }
        }
        return this.dataProxyServers;
    };

    private startDataProxyServers = async (proxyServers: BaseDataProxyServer[]): Promise<void> => {
        await Promise.allSettled(
            proxyServers.map((proxyServer) => {
                return new Promise<void>((resolve, reject) => {
                    if (proxyServer.protocol === DataProxyServerType.UDP) {
                        const server = proxyServer.server as DatagramSocket;
                        server
                            .bind({ port: proxyServer.port })
                            .on("listening", () => {
                                log.info(`[${proxyServer.name}] socket bound to localhost:${proxyServer.port}`);
                                resolve();
                            })
                            .on("error", (err) => {
                                log.error(`[${proxyServer.name}] could not bind socket. Reason: ${err.message}`);
                                reject(err);
                            });
                    } else {
                        const server = proxyServer.server as Server;
                        server
                            .listen(proxyServer.port, () => {
                                log.info(`[${proxyServer.name}] listening on localhost:${proxyServer.port}`);
                                resolve();
                            })
                            .on("error", (err) => {
                                log.error(`[${proxyServer.name}] could not start a server. Reason: ${err.message}`);
                                reject(err);
                            });
                    }
                });
            })
        );
    };
}
