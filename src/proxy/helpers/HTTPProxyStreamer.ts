import { IncomingMessage, ServerResponse } from "http";
import ProxyServer, { createProxyServer, ServerOptions } from "http-proxy";
import { config } from "../../core/config";
import { IHTTPProxyResponseOutput } from "../interfaces/IHTTPProxyResponseOutput";

export class HTTPProxyStreamer {
    private readonly proxyServer: ProxyServer;

    constructor(baseUrl: string, headers?: ServerOptions["headers"], private readonly isPrimaryTarget = false) {
        this.proxyServer = createProxyServer({
            target: baseUrl,
            headers: headers ?? {},
            timeout: config.httpProxyRequestTimeoutInMs,
            proxyTimeout: config.httpProxyResponseTimeoutInMs,
            changeOrigin: true,
            selfHandleResponse: true,
        });
    }

    public handleRequest(request: IncomingMessage, response: ServerResponse): Promise<IHTTPProxyResponseOutput> {
        return new Promise((resolve, reject) => {
            this.proxyServer.web(request, response);
            this.proxyServer.on("proxyRes", (proxyRes) => {
                let data: Uint8Array[] = [];
                proxyRes.on("data", (chunk) => {
                    if (this.isPrimaryTarget) {
                        data.push(chunk);
                    }
                });

                proxyRes.once("error", (error) => {
                    this.destroySourceStreams(request, proxyRes, error);
                    reject(error);
                });

                proxyRes.once("close", () => {
                    this.destroySourceStreams(request, proxyRes);
                    resolve({
                        statusCode: proxyRes.statusCode ?? 500,
                        headers: proxyRes.headers,
                        textBody: Buffer.concat(data).toString(),
                    });
                });

                this.proxyServer.once("close", () => {
                    this.destroySourceStreams(request, proxyRes);
                });

                this.proxyServer.once("error", (error) => {
                    this.destroySourceStreams(request, proxyRes, error);
                });
            });

            this.proxyServer.once("error", (error) => {
                reject(error);
            });
        });
    }

    /**
     * Destroy all source streams to propagate the caught event backward
     * Addressing https://github.com/http-party/node-http-proxy/issues/1586
     */
    public destroySourceStreams(request: IncomingMessage, proxyRes?: IncomingMessage, error?: Error) {
        if (!request.destroyed && this.isPrimaryTarget) {
            request.destroy(error);
        }

        if (!proxyRes?.destroyed) {
            proxyRes?.destroy(error);
        }
    }
}
