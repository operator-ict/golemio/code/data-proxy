import { IncomingHttpHeaders } from "http";

export interface IHTTPProxyResponseOutput {
    statusCode: number;
    headers: IncomingHttpHeaders;
    textBody: string;
}
