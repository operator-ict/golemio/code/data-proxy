import udp, { Socket as DatagramSocket, SocketOptions } from "dgram";
import { IUDPTargetDefinition, DataProxyServerType } from "../core/config";
import { DateUtils, log } from "../core/helpers";
import { BaseDataProxyServer } from "./BaseDataProxyServer";

export class UDPDataProxyServer extends BaseDataProxyServer {
    private static SOCKET_OPTIONS: SocketOptions = { type: "udp4" };

    public protocol = DataProxyServerType.UDP;
    public name: string;
    public port: number;
    public server: DatagramSocket;
    private targets: IUDPTargetDefinition[];
    private primaryTarget: IUDPTargetDefinition;

    constructor(id: string, name: string, port: number, targets: IUDPTargetDefinition[]) {
        super(id);
        this.name = name;
        this.port = port;
        this.targets = targets;
        this.primaryTarget = targets[0];

        this.server = udp.createSocket(UDPDataProxyServer.SOCKET_OPTIONS);
        this.server.on("message", (data) => {
            log.debug(`[${this.name}] - Data ${data.toString().substring(0, 2000)}`);

            const clientId = this.generateClientId();
            for (const target of this.targets) {
                this.forwardMessage(clientId, target, data);
            }
        });
    }

    private forwardMessage(clientId: string, target: IUDPTargetDefinition, data: Buffer) {
        const localDate = new Date();
        const localISO = DateUtils.getISODateTime(localDate);
        const isPrimaryTarget = target.host === this.primaryTarget.host && target.port === this.primaryTarget.port;
        const logPrefix = `[${this.name}] ${isPrimaryTarget ? "Primary target" : "Target"} server ${target.host}:${target.port}`;

        this.beginStatsRequest(target, {
            clientId,
            connectionEstablished: localISO,
            serverHost: target.host,
            serverPort: target.port,
        });

        const socket = udp.createSocket(UDPDataProxyServer.SOCKET_OPTIONS);
        socket.on("error", (err) => {
            log.error(`${logPrefix} error: ${err.message}`);
            this.rejectStatsRequest(target, clientId, err.message, this.computeConnectionDuration(localDate));
        });

        socket.connect(target.port, target.host, () => {
            socket.send(data, (err) => {
                if (err) {
                    log.error(`${logPrefix} error: ${err.message}`);
                    this.rejectStatsRequest(target, clientId, err.message, this.computeConnectionDuration(localDate));
                }

                try {
                    socket.disconnect();
                    this.fulfillStatsRequest(target, clientId, this.computeConnectionDuration(localDate));
                } catch (err) {
                    log.warn(`${logPrefix} error while disconnecting: ${err.message}`);
                    socket.close();
                    this.rejectStatsRequest(target, clientId, err.message, this.computeConnectionDuration(localDate));
                }
            });
        });
    }

    private computeConnectionDuration(connectionStartTime: Date) {
        const connectionEndTime = new Date();
        return +connectionEndTime - +connectionStartTime;
    }
}
