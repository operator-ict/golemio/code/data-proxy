import { Server as HttpServer } from "http";
import { Server as TcpServer } from "net";
import { Socket as DatagramSocket } from "dgram";
import crypto from "crypto";
import { IProxyStatsRequestParams, RequestResultStatus, ProxyStatsStorage } from "../domain";
import { ITCPTargetDefinition, IHTTPTargetDefinition, DataProxyServerType } from "../core/config";

type ITargetDefinition = ITCPTargetDefinition | IHTTPTargetDefinition;

export abstract class BaseDataProxyServer {
    abstract protocol: DataProxyServerType;
    abstract name: string;
    abstract port: number;
    abstract server: HttpServer | TcpServer | DatagramSocket;

    protected statsStorage: ProxyStatsStorage;

    constructor(id: string) {
        this.statsStorage = new ProxyStatsStorage(id);
    }

    protected generateClientId(): string {
        return crypto.randomBytes(8).toString("hex");
    }

    protected beginStatsRequest(target: ITargetDefinition, params: Partial<IProxyStatsRequestParams>) {
        if (!target.saveStats) {
            return;
        }

        this.statsStorage.markRequest(target.id, {
            status: RequestResultStatus.Initialized,
            clientId: params.clientId!,
            ...params,
        });
    }

    protected fulfillStatsRequest(target: ITargetDefinition, clientId: string, duration?: number) {
        if (!target.saveStats) {
            return;
        }

        this.statsStorage.markRequest(
            target.id,
            { status: RequestResultStatus.Fulfilled, clientId, connectionDuration: duration },
            true
        );
    }

    protected rejectStatsRequest(target: ITargetDefinition, clientId: string, error: string, duration?: number) {
        if (!target.saveStats) {
            return;
        }

        this.statsStorage.markRequest(
            target.id,
            { status: RequestResultStatus.Rejected, clientId, connectionDuration: duration, error },
            true
        );
    }

    protected timeoutStatsRequest(target: ITargetDefinition, clientId: string, duration?: number) {
        if (!target.saveStats) {
            return;
        }

        this.statsStorage.markRequest(
            target.id,
            { status: RequestResultStatus.TimedOut, clientId, connectionDuration: duration },
            true
        );
    }
}
