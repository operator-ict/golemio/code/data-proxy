import { createServer, IncomingMessage, Server, ServerResponse } from "http";
import { DataProxyServerType, IHTTPTargetDefinition } from "../core/config";
import { log } from "../core/helpers";
import { BaseDataProxyServer } from "./BaseDataProxyServer";
import { HTTPProxyStreamer } from "./helpers/HTTPProxyStreamer";
import { IHTTPProxyResponseOutput } from "./interfaces/IHTTPProxyResponseOutput";

export class HTTPDataProxyServer extends BaseDataProxyServer {
    public protocol = DataProxyServerType.HTTP;
    public name: string;
    public port: number;
    public server: Server;
    private targets: IHTTPTargetDefinition[];

    constructor(id: string, name: string, port: number, targets: IHTTPTargetDefinition[]) {
        super(id);
        this.name = name;
        this.port = port;
        this.targets = targets;

        this.server = createServer((request, response) => {
            this.duplicateRequest(request, response);
        });
    }

    private async duplicateRequest(request: IncomingMessage, response: ServerResponse) {
        const clientId = this.generateClientId();
        const connectionStartTime = new Date();

        let proxyOutputs: Array<Promise<IHTTPProxyResponseOutput>> = [];

        // Create proxy request to each target
        for (let i = 0; i < this.targets.length; i++) {
            const target = this.targets[i];
            const isPrimaryTarget = i === 0;
            const targetPrefix = isPrimaryTarget ? "Primary target" : "Target";

            this.beginStatsRequest(target, {
                clientId,
                connectionEstablished: connectionStartTime.toISOString(),
                serverUrl: `${target.url}${request.url}`,
                serverRequestMethod: request.method,
            });

            const proxyStreamer = new HTTPProxyStreamer(target.url, target.headers, isPrimaryTarget);
            const proxyOutput = proxyStreamer
                .handleRequest(request, response)
                .then((proxyOutput) => {
                    this.fulfillStatsRequest(target, clientId, this.computeDuration(connectionStartTime));
                    return proxyOutput;
                })
                .catch((error) => {
                    this.rejectStatsRequest(target, clientId, error, this.computeDuration(connectionStartTime));
                    log.error(`[${this.name}] ${targetPrefix} ${target.url}${request.url} error: ${error.message}`);
                    throw error;
                });

            proxyOutputs.push(proxyOutput);
        }

        // Settle all requests asynchronously to avoid unhandled rejections
        Promise.allSettled(proxyOutputs);

        try {
            // Proxy data from target 1 back to source...
            const { statusCode, headers, textBody } = await proxyOutputs[0];
            response.writeHead(statusCode, headers);
            response.write(textBody);
        } catch (error) {
            log.error(error);

            // ...or return 503
            response.statusCode = 503;
            response.write(error.message);
        } finally {
            response.end();
        }
    }

    private computeDuration(startTime: Date) {
        return new Date().getTime() - startTime.getTime();
    }
}
