import multiWriteStream from "multi-write-stream";
import { createConnection, createServer, Server, Socket } from "net";
import { Writable } from "stream";
import { ITCPTargetDefinition, DataProxyServerType, config } from "../core/config";
import { DateUtils, log } from "../core/helpers";
import { BaseDataProxyServer } from "./BaseDataProxyServer";

interface ITCPClient {
    connectionStartTime?: number;
    clientSocket: Socket;
    serverSockets: Socket[];
}

export class TCPDataProxyServer extends BaseDataProxyServer {
    public protocol = DataProxyServerType.TCP;
    public name: string;
    public port: number;
    public server: Server;
    private targets: ITCPTargetDefinition[];
    private primaryTarget: ITCPTargetDefinition;
    private clients: Map<string, ITCPClient>;

    constructor(id: string, name: string, port: number, targets: ITCPTargetDefinition[]) {
        super(id);
        this.name = name;
        this.port = port;
        this.targets = targets;
        this.primaryTarget = targets[0];
        this.clients = new Map();

        this.server = createServer((clientSocket) => {
            const clientId = this.generateClientId();
            const serverSockets = this.targets.map((target, i) => this.createServerConnection(clientId, target, i));

            // save client with active server connections
            this.clients.set(clientId, { clientSocket, serverSockets });

            // proxy data from source to all targets
            clientSocket.pipe(
                multiWriteStream(serverSockets, {
                    write: this.handleStreamWrite,
                    autoDestroy: false,
                })
            );

            clientSocket.on("data", (data) => {
                log.debug(`[${this.name}] - Data ${data.toString()}`);
            });

            clientSocket.on("error", (err) => {
                log.error(`[${this.name}] Client connection failure: ${err.message}`);
            });

            clientSocket.on("close", () => {
                const duration = this.computeConnectionDuration(clientId);

                // connection to data proxy has closed, destroy every connection
                this.destroySocketStreams(clientId);

                for (const target of this.targets) {
                    this.fulfillStatsRequest(target, clientId, duration);
                }
            });

            // proxy data from target server 1 back to source
            serverSockets[0].pipe(clientSocket);
        });
    }

    /**
     * Create new connection to a target server (see dataProxyServerDefinitions)
     */
    private createServerConnection(
        clientId: string,
        target: ITCPTargetDefinition,
        targetIndex: number,
        connectionCounter = 0
    ): Socket {
        const socket = createConnection(target);
        const isPrimaryTarget = target.host === this.primaryTarget.host && target.port === this.primaryTarget.port;
        const logPrefix = `[${this.name}] ${isPrimaryTarget ? "Primary target" : "Target"} server ${target.host}:${target.port}`;

        socket.setTimeout(config.tcpSocketTimeoutInMs);
        socket.on("connect", () => {
            const localDate = new Date();
            const localISO = DateUtils.getISODateTime(localDate);
            const client = this.clients.get(clientId);
            if (client) client.connectionStartTime = localDate.valueOf();

            this.beginStatsRequest(target, {
                clientId,
                connectionEstablished: localISO,
                serverHost: target.host,
                serverPort: target.port,
                reconnectAttempts: connectionCounter,
            });

            // reset connection counter after connecting for at least 3 seconds
            setTimeout(() => {
                connectionCounter = 0;
            }, 3000);
        });

        socket.on("error", (err) => {
            log.error(`${logPrefix} error: ${err.message}`);

            const duration = this.computeConnectionDuration(clientId);
            this.rejectStatsRequest(target, clientId, err.message, duration);
        });

        socket.on("timeout", () => {
            log.error(`${logPrefix} timed out`);

            const duration = this.computeConnectionDuration(clientId);
            this.timeoutStatsRequest(target, clientId, duration);
            socket.destroy();
        });

        socket.on("close", () => {
            // connection to the primary target server has closed, destroy all connections
            if (isPrimaryTarget) {
                const duration = this.computeConnectionDuration(clientId);
                this.rejectStatsRequest(target, clientId, "Primary connection lost", duration);
                this.destroySocketStreams(clientId);
                return;
            }

            // attempt to reconnect to a target secondary server
            setTimeout(() => {
                const client = this.clients.get(clientId);
                if (!client || client.clientSocket.destroyed) return;

                if (connectionCounter === config.secondaryTargetReconnectMaxAttempts) {
                    const duration = this.computeConnectionDuration(clientId);
                    this.rejectStatsRequest(target, clientId, "Too many reconnect attempts", duration);
                    this.destroySocketStreams(clientId);
                    return;
                }

                log.warn(
                    `${logPrefix} connection failed, reconnecting (${connectionCounter + 1}/${
                        config.secondaryTargetReconnectMaxAttempts
                    })`
                );

                const newSocket = this.createServerConnection(clientId, target, targetIndex, connectionCounter + 1);
                client.serverSockets[targetIndex] = newSocket;
            }, config.secondaryTargetReconnectTimeout);
        });

        return socket;
    }

    /**
     * Destroy every connection associated with given client id
     */
    private destroySocketStreams(clientId: string) {
        const client = this.clients.get(clientId);
        if (!client) return;

        log.debug(`[${this.name}] Destroying all connections for ${clientId}`);
        this.clients.delete(clientId);
        for (const serverSocket of client.serverSockets) {
            if (!serverSocket.destroyed) {
                serverSocket.end(() => {
                    serverSocket.destroy();
                });
            }
        }

        if (!client.clientSocket.destroyed) {
            client.clientSocket.end(() => {
                client.clientSocket.destroy();
            });
        }
    }

    /**
     * Override the internal _write function from multi-write-stream to better handle disconnects
     */
    private handleStreamWrite(this: Writable & { streams: Socket[] }, data: Buffer, _encoding: string, callback: () => void) {
        // stop if the stream is already destroyed
        // or if the end signal (<Buffer 00>) is received
        if (this.destroyed || data.equals(Buffer.from([0]))) return callback();
        for (const socket of this.streams) {
            socket.write(data);
        }

        callback();
    }

    private computeConnectionDuration(clientId: string): number | undefined {
        const client = this.clients.get(clientId);
        let duration: number | undefined;

        if (client && client.connectionStartTime) {
            const connectionEndTime = new Date();
            duration = connectionEndTime.valueOf() - client.connectionStartTime;
        }

        return duration;
    }
}
