import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { DataProxyServerType } from "../../src/core/config";
import { BaseDataProxyServer } from "../../src/proxy/BaseDataProxyServer";

class DataProxyServer extends BaseDataProxyServer {
    protocol = DataProxyServerType.TCP;
    name = "test";
    port = 6969;
    server = "dummy" as any;
}

describe("BaseDataProxyServer", () => {
    let sandbox: SinonSandbox;
    let markRequestMock: SinonStub;
    let proxyServer: DataProxyServer;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        proxyServer = new DataProxyServer("test");
        markRequestMock = sandbox.stub(proxyServer["statsStorage"], "markRequest");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("generateClientId should generate string", () => {
        const id = proxyServer["generateClientId"]();
        expect(id.length).to.eq(16);
    });

    it("beginStatsRequest should not do anything", () => {
        proxyServer["beginStatsRequest"]({} as any, {});
        expect(markRequestMock.callCount).to.eq(0);
    });

    it("beginStatsRequest should call markRequest", () => {
        proxyServer["beginStatsRequest"]({ saveStats: true, id: "test-target" } as any, { clientId: "test-client" });
        expect(markRequestMock.getCall(0).args).to.deep.eq(["test-target", { status: "initialized", clientId: "test-client" }]);
    });

    it("fulfillStatsRequest should not do anything", () => {
        proxyServer["fulfillStatsRequest"]({} as any, "test-client");
        expect(markRequestMock.callCount).to.eq(0);
    });

    it("fulfillStatsRequest should call markRequest", () => {
        proxyServer["fulfillStatsRequest"]({ saveStats: true, id: "test-target" } as any, "test-client", 69);
        expect(markRequestMock.getCall(0).args).to.deep.eq([
            "test-target",
            { status: "fulfilled", clientId: "test-client", connectionDuration: 69 },
            true,
        ]);
    });

    it("rejectStatsRequest should not do anything", () => {
        proxyServer["rejectStatsRequest"]({} as any, "test-client", "");
        expect(markRequestMock.callCount).to.eq(0);
    });

    it("rejectStatsRequest should call markRequest", () => {
        proxyServer["rejectStatsRequest"]({ saveStats: true, id: "test-target" } as any, "test-client", "test-error", 69);
        expect(markRequestMock.getCall(0).args).to.deep.eq([
            "test-target",
            { status: "rejected", clientId: "test-client", error: "test-error", connectionDuration: 69 },
            true,
        ]);
    });

    it("timeoutStatsRequest should not do anything", () => {
        proxyServer["timeoutStatsRequest"]({} as any, "test-client");
        expect(markRequestMock.callCount).to.eq(0);
    });

    it("timeoutStatsRequest should call markRequest", () => {
        proxyServer["timeoutStatsRequest"]({ saveStats: true, id: "test-target" } as any, "test-client", 69);
        expect(markRequestMock.getCall(0).args).to.deep.eq([
            "test-target",
            { status: "timed-out", clientId: "test-client", connectionDuration: 69 },
            true,
        ]);
    });
});
