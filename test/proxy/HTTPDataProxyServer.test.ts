import express from "@golemio/core/dist/shared/express";
import { expect } from "chai";
import { createServer, Server } from "http";
import * as sinon from "sinon";
import supertest from "supertest";
import { log } from "../../src/core/helpers";
import { HTTPDataProxyServer } from "../../src/proxy";
import { setTimeout as sleep } from "timers/promises";

describe("HTTPDataProxyServer", () => {
    let sandbox: sinon.SinonSandbox;
    let primaryFunc: sinon.SinonStub;
    let secondaryFunc: sinon.SinonStub;
    let errorLogStub: sinon.SinonStub;
    let proxy: HTTPDataProxyServer;
    let primaryTargetServer: Server;
    let secondaryTargetServer: Server;

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        primaryFunc = sandbox.stub();
        secondaryFunc = sandbox.stub();
        errorLogStub = sandbox.stub(log, "error");

        proxy = new HTTPDataProxyServer("test-http", "test http", 40000, [
            { id: "first", url: "http://127.0.0.1:40001" },
            { id: "second", url: "http://127.0.0.1:40002", headers: { "x-test": "test" } },
        ]);

        const primaryApp = express();
        primaryApp.use(express.raw({ type: "*/*" }));
        primaryApp.post("/test", (req, res, next) => {
            primaryFunc({ data: req.body, headers: req.headers });
            res.setHeader("x-foo", "bar");
            res.json({ test: "ok" });
        });
        primaryTargetServer = createServer(primaryApp);
        await new Promise<void>((done) =>
            primaryTargetServer.listen(40001, () => {
                log.info("Test primary server started.");
                done();
            })
        );

        const secondaryApp = express();
        secondaryApp.use(express.raw({ type: "*/*" }));
        secondaryApp.post("/test", (req, res, next) => {
            secondaryFunc({ data: req.body, headers: req.headers });
            res.sendStatus(204);
        });
        secondaryTargetServer = createServer(secondaryApp);
        await new Promise<void>((done) =>
            secondaryTargetServer.listen(40002, () => {
                log.info("Test secondary server started.");
                done();
            })
        );
    });

    afterEach(async () => {
        sandbox.restore();
        await new Promise((done) => primaryTargetServer.close(done));
        await new Promise((done) => secondaryTargetServer.close(done));
    });

    it("should duplicate request to all targets", (done) => {
        supertest(proxy.server)
            .post("/test")
            .send("<test>data</test>")
            .end((err, res) => {
                if (err) {
                    log.error(err);
                    done(err);
                }

                sandbox.assert.calledOnce(primaryFunc);
                sandbox.assert.calledWith(primaryFunc, {
                    data: Buffer.from("<test>data</test>"),
                    headers: {
                        connection: "close",
                        "content-length": "17",
                        "content-type": "application/x-www-form-urlencoded",
                        "accept-encoding": "gzip, deflate",
                        host: "127.0.0.1:40001",
                    },
                });
                sandbox.assert.calledOnce(secondaryFunc);
                sandbox.assert.calledWith(secondaryFunc, {
                    data: Buffer.from("<test>data</test>"),
                    headers: {
                        connection: "close",
                        "content-length": "17",
                        "content-type": "application/x-www-form-urlencoded",
                        "accept-encoding": "gzip, deflate",
                        host: "127.0.0.1:40002",
                        "x-test": "test",
                    },
                });

                expect(res.headers).to.have.property("x-foo");
                expect(res.body).to.deep.equal({ test: "ok" });
                done();
            });
    });

    it("should duplicate request to primary target and log secondary target unavailable", (done) => {
        secondaryTargetServer.close(() => {
            supertest(proxy.server)
                .post("/test")
                .send("<test>data</test>")
                .end((err, res) => {
                    if (err) {
                        log.error(err);
                        done(err);
                    }

                    sandbox.assert.calledOnce(primaryFunc);
                    sandbox.assert.calledWith(primaryFunc, {
                        data: Buffer.from("<test>data</test>"),
                        headers: {
                            connection: "close",
                            "content-length": "17",
                            "content-type": "application/x-www-form-urlencoded",
                            "accept-encoding": "gzip, deflate",
                            host: "127.0.0.1:40001",
                        },
                    });

                    sandbox.assert.calledOnce(errorLogStub);
                    sandbox.assert.calledWith(
                        errorLogStub,
                        "[test http] Target http://127.0.0.1:40002/test error: connect ECONNREFUSED 127.0.0.1:40002"
                    );

                    expect(res.headers).to.have.property("x-foo");
                    expect(res.body).to.deep.equal({ test: "ok" });
                    done();
                });
        });
    });

    it("should duplicate request to secondary target and log primary target unavailable", (done) => {
        primaryTargetServer.close(() => {
            supertest(proxy.server)
                .post("/test")
                .send("<test>data</test>")
                .end(async (err, res) => {
                    if (err) {
                        log.error(err);
                        done(err);
                    }

                    // Proxy requests to secondary targets are asynchronous
                    await sleep(500);

                    sandbox.assert.calledOnce(secondaryFunc);
                    sandbox.assert.calledWith(secondaryFunc, {
                        data: Buffer.from("<test>data</test>"),
                        headers: {
                            connection: "close",
                            "content-length": "17",
                            "content-type": "application/x-www-form-urlencoded",
                            "accept-encoding": "gzip, deflate",
                            host: "127.0.0.1:40002",
                            "x-test": "test",
                        },
                    });

                    sandbox.assert.calledTwice(errorLogStub);
                    sandbox.assert.calledWith(
                        errorLogStub.firstCall,
                        "[test http] Primary target http://127.0.0.1:40001/test error: connect ECONNREFUSED 127.0.0.1:40001"
                    );

                    expect(res.status).to.be.equal(503);
                    done();
                });
        });
    });
});
