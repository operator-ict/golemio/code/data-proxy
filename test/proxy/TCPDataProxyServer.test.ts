import { expect } from "chai";
import { createConnection, createServer, Server } from "net";
import * as sinon from "sinon";
import { TCPDataProxyServer } from "../../src/proxy";
import { log } from "../../src/core/helpers";
import { config } from "../../src/core/config";

describe("TCPDataProxyServer", () => {
    let sandbox: sinon.SinonSandbox;
    let primaryFunc: sinon.SinonStub;
    let secondaryFunc: sinon.SinonStub;
    let errorLogStub: sinon.SinonStub;
    let proxy: TCPDataProxyServer;
    let primaryTargetServer: Server;
    let secondaryTargetServer: Server;

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        primaryFunc = sandbox.stub();
        secondaryFunc = sandbox.stub();
        errorLogStub = sandbox.stub(log, "error");

        config.secondaryTargetReconnectMaxAttempts = 5;
        config.secondaryTargetReconnectTimeout = 300;

        proxy = new TCPDataProxyServer("test-tcp", "test tcp", 30000, [
            { id: "first", host: "127.0.0.1", port: 30001 },
            { id: "second", host: "127.0.0.1", port: 30002 },
        ]);

        primaryTargetServer = createServer((socket) => {
            socket.on("data", (data) => {
                primaryFunc(data);
                socket.write(`ACK data received\r\n`);
            });
        });
        await new Promise<void>((done) =>
            primaryTargetServer
                .listen(30001, () => {
                    log.info("Test primary server started.");
                    done();
                })
                .on("close", () => {
                    log.info("Test primary server closed.");
                })
        );

        secondaryTargetServer = createServer((socket) => {
            socket.on("data", (data) => {
                secondaryFunc(data);
                socket.write(`ACK data received\r\n`);
            });
        });
        await new Promise<void>((done) =>
            secondaryTargetServer
                .listen(30002, () => {
                    log.info("Test secondary server started.");
                    done();
                })
                .on("close", () => {
                    log.info("Test secondary server closed.");
                })
        );

        await new Promise<void>((done) => {
            proxy.server
                .listen(30000, () => {
                    log.info("Proxy started.");
                    done();
                })
                .on("close", () => {
                    log.info("Proxy closed.");
                });
        });
    });

    afterEach(async () => {
        await new Promise((done) => primaryTargetServer.close(done));
        await new Promise((done) => secondaryTargetServer.close(done));
        await new Promise((done) => proxy.server.close(done));
        sandbox.restore();
    });

    it("should duplicate request to all targets", (done) => {
        let responseData: Buffer;
        const socket = createConnection({ host: "127.0.0.1", port: 30000 }, () => {
            socket.write("<test>data</test>");
        });
        socket.on("data", (data) => {
            responseData = data;
            socket.end();
        });

        setTimeout(() => {
            expect(responseData.toString()).to.be.equal(`ACK data received\r\n`);

            sandbox.assert.calledOnce(primaryFunc);
            sandbox.assert.calledWith(primaryFunc, Buffer.from("<test>data</test>"));

            sandbox.assert.calledOnce(secondaryFunc);
            sandbox.assert.calledWith(secondaryFunc, Buffer.from("<test>data</test>"));

            done();
        }, 1000);
    });

    it("should reconnect to secondary target and duplicate request to all targets", (done) => {
        secondaryTargetServer.close(async (err) => {
            if (err) done(err);

            let responseData: Buffer;
            const socket = createConnection({ host: "127.0.0.1", port: 30000 });
            socket.on("data", (data) => {
                responseData = data;
                socket.end();
            });

            secondaryTargetServer = createServer((clientSocket) => {
                socket.write("<test>data</test>");
                clientSocket.on("data", (data) => {
                    secondaryFunc(data);
                    clientSocket.write(`ACK data received\r\n`);
                });
            });

            await new Promise<void>((done) =>
                secondaryTargetServer
                    .listen(30002, () => {
                        log.info("Test secondary server started.");
                        done();
                    })
                    .on("close", () => {
                        log.info("Test secondary server closed.");
                    })
            );

            setTimeout(() => {
                expect(responseData.toString()).to.be.equal(`ACK data received\r\n`);

                sandbox.assert.calledOnce(primaryFunc);
                sandbox.assert.calledWith(primaryFunc, Buffer.from("<test>data</test>"));

                sandbox.assert.calledOnce(secondaryFunc);
                sandbox.assert.calledWith(secondaryFunc, Buffer.from("<test>data</test>"));

                done();
            }, 3000);
        });
    });

    it("should duplicate request to primary target and log secondary target unavailable", (done) => {
        secondaryTargetServer.close((err) => {
            if (err) done(err);

            let responseData: Buffer;
            const socket = createConnection({ host: "127.0.0.1", port: 30000 }, () => {
                socket.write("<test>data</test>");
            });
            socket.on("data", (data) => {
                responseData = data;
                socket.end();
            });

            setTimeout(() => {
                expect(responseData.toString()).to.be.equal(`ACK data received\r\n`);

                sandbox.assert.calledOnce(primaryFunc);
                sandbox.assert.calledWith(primaryFunc, Buffer.from("<test>data</test>"));

                sandbox.assert.calledOnce(errorLogStub);
                sandbox.assert.calledWith(
                    errorLogStub,
                    "[test tcp] Target server 127.0.0.1:30002 error: connect ECONNREFUSED 127.0.0.1:30002"
                );

                done();
            }, 2000);
        });
    });

    it("should duplicate request to secondary target and log primary target unavailable", (done) => {
        primaryTargetServer.close((err) => {
            if (err) done(err);

            let responseData: Buffer;
            const socket = createConnection({ host: "127.0.0.1", port: 30000 }, () => {
                socket.write("<test>data</test>");
            });
            socket.on("data", (data) => {
                responseData = data;
                socket.end();
            });

            setTimeout(() => {
                expect(responseData).to.be.undefined;

                sandbox.assert.calledOnce(secondaryFunc);
                sandbox.assert.calledWith(secondaryFunc, Buffer.from("<test>data</test>"));

                sandbox.assert.calledOnce(errorLogStub);
                sandbox.assert.calledWith(
                    errorLogStub,
                    "[test tcp] Primary target server 127.0.0.1:30001 error: connect ECONNREFUSED 127.0.0.1:30001"
                );

                done();
            }, 2000);
        });
    });

    it("should disconnect client after losing connection to primary target", (done) => {
        let isConnectionClosed = false;
        const socket = createConnection({ host: "127.0.0.1", port: 30000 });
        socket.on("close", (hadError) => {
            if (hadError) done(new Error("Client connection error"));
            isConnectionClosed = true;
        });

        primaryTargetServer.close();

        setTimeout(() => {
            expect(isConnectionClosed).to.equal(true);
            done();
        }, 1000);
    });

    it("should disconnect client after losing connection to secondary target (failed reconnection)", (done) => {
        let isConnectionClosed = false;
        const socket = createConnection({ host: "127.0.0.1", port: 30000 });

        socket.on("close", (hadError) => {
            if (hadError) done(new Error("Client connection error"));
            isConnectionClosed = true;
        });

        secondaryTargetServer.close();

        setTimeout(() => {
            expect(isConnectionClosed).to.equal(true);
            done();
        }, 3000);
    });
});
