import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import udp, { Socket as DatagramSocket } from "dgram";
import { UDPDataProxyServer } from "../../src/proxy";
import { log } from "../../src/core/helpers";

describe("UDPDataProxyServer", () => {
    let sandbox: SinonSandbox;
    let primaryFunc: SinonStub;
    let secondaryFunc: SinonStub;
    let errorLogStub: SinonStub;
    let proxy: UDPDataProxyServer;
    let primaryTargetServer: DatagramSocket;
    let secondaryTargetServer: DatagramSocket;

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        primaryFunc = sandbox.stub();
        secondaryFunc = sandbox.stub();
        errorLogStub = sandbox.stub(log, "error");

        proxy = new UDPDataProxyServer("test-udp", "test udp", 30000, [
            { id: "first", host: "127.0.0.1", port: 30001 },
            { id: "second", host: "127.0.0.1", port: 30002 },
        ]);

        primaryTargetServer = udp.createSocket(UDPDataProxyServer["SOCKET_OPTIONS"]).on("message", (data) => {
            primaryFunc(data);
        });

        await new Promise<void>((resolve) =>
            primaryTargetServer
                .bind({ port: 30001 }, () => {
                    log.info("Test primary server started");
                    resolve();
                })
                .on("close", () => {
                    log.info("Test primary server closed");
                })
        );

        secondaryTargetServer = udp.createSocket(UDPDataProxyServer["SOCKET_OPTIONS"]).on("message", (data) => {
            secondaryFunc(data);
        });

        await new Promise<void>((resolve) =>
            secondaryTargetServer
                .bind({ port: 30002 }, () => {
                    log.info("Test secondary server started");
                    resolve();
                })
                .on("close", () => {
                    log.info("Test secondary server closed");
                })
        );

        await new Promise<void>((resolve) => {
            proxy.server
                .bind({ port: 30000 }, () => {
                    log.info("Proxy started");
                    resolve();
                })
                .on("close", () => {
                    log.info("Proxy closed");
                });
        });
    });

    afterEach(async () => {
        sandbox.restore();

        try {
            await new Promise<void>((resolve) => primaryTargetServer.close(resolve));
        } catch (err) {}

        try {
            await new Promise<void>((resolve) => secondaryTargetServer.close(resolve));
        } catch (err) {}

        await new Promise<void>((resolve) => proxy.server.close(resolve));
    });

    it("should forward a message to all targets", (done) => {
        const socket = udp.createSocket(UDPDataProxyServer["SOCKET_OPTIONS"]);
        socket.connect(30000, "127.0.0.1", () => {
            socket.send("<test>data</test>", () => {
                socket.disconnect();
                socket.close();
            });
        });

        setTimeout(() => {
            sandbox.assert.calledOnce(primaryFunc);
            sandbox.assert.calledWith(primaryFunc, Buffer.from("<test>data</test>"));
            sandbox.assert.calledOnce(secondaryFunc);
            sandbox.assert.calledWith(secondaryFunc, Buffer.from("<test>data</test>"));
            done();
        }, 500);
    });

    it("should duplicate request to primary target and log secondary target unavailable", (done) => {
        secondaryTargetServer.close(() => {
            const socket = udp.createSocket(UDPDataProxyServer["SOCKET_OPTIONS"]);
            socket.connect(30000, "127.0.0.1", () => {
                socket.send("<test>data</test>", () => {
                    socket.disconnect();
                    socket.close();
                });
            });

            setTimeout(() => {
                expect(secondaryFunc.callCount).to.equal(0);

                sandbox.assert.calledOnce(primaryFunc);
                sandbox.assert.calledWith(primaryFunc, Buffer.from("<test>data</test>"));

                sandbox.assert.calledOnce(errorLogStub);
                sandbox.assert.calledWith(errorLogStub, "[test udp] Target server 127.0.0.1:30002 error: recvmsg ECONNREFUSED");

                done();
            }, 500);
        });
    });

    it("should duplicate request to secondary target and log primary target unavailable", (done) => {
        primaryTargetServer.close(() => {
            const socket = udp.createSocket(UDPDataProxyServer["SOCKET_OPTIONS"]);
            socket.connect(30000, "127.0.0.1", () => {
                socket.send("<test>data</test>", () => {
                    socket.disconnect();
                    socket.close();
                });
            });

            setTimeout(() => {
                expect(primaryFunc.callCount).to.equal(0);

                sandbox.assert.calledOnce(secondaryFunc);
                sandbox.assert.calledWith(secondaryFunc, Buffer.from("<test>data</test>"));

                sandbox.assert.calledOnce(errorLogStub);
                sandbox.assert.calledWith(
                    errorLogStub,
                    "[test udp] Primary target server 127.0.0.1:30001 error: recvmsg ECONNREFUSED"
                );

                done();
            }, 500);
        });
    });
});
