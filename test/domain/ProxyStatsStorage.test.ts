import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import { RequestResultStatus } from "../../src/domain/abstract";
import { ProxyStatsStorage } from "../../src/domain/ProxyStatsStorage";

describe("ProxyStatsStorage", () => {
    let sandbox: SinonSandbox;
    let proxyStatsStorage: ProxyStatsStorage;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        proxyStatsStorage = new ProxyStatsStorage("test");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("resetStatsBuffer should clear the cache", () => {
        proxyStatsStorage.statsBuffer.requests = { test: [{ status: RequestResultStatus.Fulfilled, clientId: "test-client" }] };
        for (let i = 0; i < 1001; i++) {
            proxyStatsStorage["requestHolder"].set("test-client-" + i, {
                status: RequestResultStatus.Initialized,
                clientId: "test-client-" + i,
            });
        }

        proxyStatsStorage.resetStatsBuffer();
        expect(Object.keys(proxyStatsStorage.statsBuffer.requests).length).to.eq(0);
        expect(proxyStatsStorage["requestHolder"].size).to.eq(0);
    });

    it("markRequest should not do anything", () => {
        const holderSetMock = sandbox.stub(proxyStatsStorage["requestHolder"], "set");
        sandbox.stub(proxyStatsStorage["requestHolder"], "get").returns(undefined);

        proxyStatsStorage.markRequest("test-server", { status: RequestResultStatus.Fulfilled, clientId: "test-client" }, true);
        expect(holderSetMock.callCount).to.eq(0);
    });

    it("markRequest should add request to the holder", () => {
        const holderSetMock = sandbox.stub(proxyStatsStorage["requestHolder"], "set");
        sandbox.stub(proxyStatsStorage["requestHolder"], "get").returns({} as any);
        const bufferRequestMock = sandbox.stub(proxyStatsStorage, "bufferRequest" as any);

        proxyStatsStorage.markRequest("test-server", { status: RequestResultStatus.Fulfilled, clientId: "test-client" }, true);
        expect(holderSetMock.getCall(0).args).to.deep.eq([
            "test-server-test-client",
            { status: "fulfilled", clientId: "test-client" },
        ]);

        expect(bufferRequestMock.getCall(0).args).to.deep.eq(["test-server", "test-client"]);
    });

    it("bufferRequest should modify statsBuffer", () => {
        sandbox.stub(proxyStatsStorage["requestHolder"], "get").returns("test" as any);

        proxyStatsStorage["bufferRequest"]("test-server", "test-client");
        expect(proxyStatsStorage.statsBuffer.requests["test-server"][0]).to.eq("test");
    });
});
