import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import { AbstractStorageService } from "@golemio/core/dist/helpers/data-access/storage";
import { config } from "../../src/core/config";
import { StorageService } from "../../src/domain/StorageService";

describe("StorageService", () => {
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("getInstance should return an instance of service", () => {
        sandbox.stub(config, "storage").value({
            enabled: true,
            provider: {
                azure: {
                    clientId: "client-id",
                    tenantId: "tenant-id",
                    clientSecret: "secret",
                    account: "test",
                    containerName: "local",
                },
            },
        });

        const service = StorageService.getInstance();
        expect(service).to.be.instanceOf(AbstractStorageService);
    });
});
