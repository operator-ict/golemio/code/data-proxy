import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { StorageService } from "../../src/domain/StorageService";
import { proxyStatsCronService } from "../../src/domain/ProxyStatsCronService";

describe("ProxyStatsCronService", () => {
    let sandbox: SinonSandbox;
    let cronStopMock: SinonStub;
    let uploadFileMock: SinonStub;
    let storageObjects: Set<any>;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        cronStopMock = sinon.stub();
        uploadFileMock = sandbox.stub();
        storageObjects = new Set();

        sandbox.stub(proxyStatsCronService, "storageObjects" as any).value(storageObjects);
        sandbox.stub(proxyStatsCronService, "cronJob" as any).value({ stop: cronStopMock });
        sandbox.stub(StorageService, "getInstance" as any).returns({ uploadFile: uploadFileMock });
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("deactivate should stop cron job", () => {
        proxyStatsCronService.deactivate();
        expect(cronStopMock.callCount).to.eq(1);
    });

    it("addStorage should add storage object", () => {
        proxyStatsCronService.addStorage("test" as any);
        expect(Array.from(storageObjects)[0]).to.eq("test");
    });

    it("collectAndSaveStats should return a function", async () => {
        const descriptor = proxyStatsCronService["collectAndSaveStats"]();
        expect(descriptor).to.be.instanceOf(Function);
    });

    it("collectAndSaveStats descriptor should not do anything", async () => {
        const storage = {
            hasStatsChanged: sinon.stub().returns(false),
        };

        storageObjects.add(storage);

        const descriptor = proxyStatsCronService["collectAndSaveStats"]();
        await descriptor();

        expect(uploadFileMock.callCount).to.eq(0);
    });

    it("collectAndSaveStats descriptor should save data to storage", async () => {
        const storage = {
            statsBuffer: {},
            proxyServerId: "test",
            resetStatsBuffer: sinon.stub(),
            hasStatsChanged: sinon.stub().returns(true),
        };

        storageObjects.add(storage);

        const descriptor = proxyStatsCronService["collectAndSaveStats"]();
        await descriptor();

        expect(storage.resetStatsBuffer.callCount).to.eq(1);
        expect(uploadFileMock.getCall(0).args).to.deep.eq(["{}", "test", "json"]);
    });
});
