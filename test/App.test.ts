import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import "mocha";
import request, { Response } from "supertest";
import sinon, { SinonSandbox } from "sinon";
import express from "@golemio/core/dist/shared/express";
import { config, DataProxyServerType } from "../src/core/config";
import { log } from "../src/core/helpers";
import { proxyStatsCronService } from "../src/domain";
import App from "../src/App";

chai.use(chaiAsPromised);

describe("App", () => {
    let expressApp: express.Application;
    let app: App;
    let sandbox: SinonSandbox;

    before(async () => {
        sandbox = sinon.createSandbox();
        sandbox.stub(process, "exit");

        config.dataProxyServerDefinitions = [];

        app = new App();
        await app.start();
        expressApp = app.express;
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should start", async () => {
        expect(expressApp).not.to.be.undefined;
    });

    it("should have all config variables set", () => {
        expect(config).not.to.be.undefined;
        expect(config.dataProxyServerDefinitions).not.to.be.undefined;
    });

    it("should have health check on /", (done) => {
        request(expressApp).get("/").set("Accept", "application/json").expect("Content-Type", /json/).expect(200, done);
    });

    it("should have health check on /health-check", (done) => {
        request(expressApp)
            .get("/healthcheck")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect((res: Response) => {
                expect(res.body).to.deep.include({
                    app: "Golemio Data Platform TCP/HTTP Data Proxy",
                    health: true,
                    services: [],
                });
            })
            .expect(200, done);
    });

    it("should return 404 on non-existing route /non-existing", (done) => {
        request(expressApp)
            .get("/non-existing")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(404, done);
    });

    it("gracefulShutdown should close all servers", async () => {
        const cronStopStub = sandbox.stub(proxyStatsCronService, "deactivate");

        const dummyServer = {
            close: sandbox.stub().callsFake((cb) => {
                cb();
            }),
        };

        const dummyProxyServer = {
            server: {
                close: sandbox.stub().callsFake((cb) => {
                    cb();
                }),
            },
        };

        app["server"] = dummyServer as any;
        app["dataProxyServers"] = [dummyProxyServer as any];

        await app["gracefulShutdown"]();
        expect(dummyServer.close.callCount).to.equal(1);
        expect(dummyProxyServer.server.close.callCount).to.equal(1);
        expect(cronStopStub.callCount).to.equal(1);
    });

    it("initDataProxyServers should return server instances", () => {
        const warnStub = sandbox.stub(log, "warn");
        const definitions = [
            {
                id: "tcp-server",
                name: "tcp server",
                type: DataProxyServerType.TCP,
                port: 30000,
                targets: [{ id: "first-target", host: "127.0.0.1", port: 31000 }],
            },
            {
                id: "udp-server",
                name: "udp server",
                type: DataProxyServerType.UDP,
                port: 30001,
                targets: [{ id: "first-target", host: "127.0.0.1", port: 31001 }],
            },
            {
                id: "http-server",
                name: "http server",
                type: DataProxyServerType.HTTP,
                port: 30002,
                targets: [{ id: "first-target", url: "http://127.0.0.1:4001" }],
            },
            {
                id: "bogus-server",
                name: "bogus server",
                type: "bogus",
            },
        ];

        const servers = app["initDataProxyServers"](definitions as any);
        expect(servers.length).to.equal(definitions.length - 1);
        expect(warnStub.getCall(0).args[0]).to.equal("Undefined server definition type. bogus server bogus");
    });
});
