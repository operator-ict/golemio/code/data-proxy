import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import { DateUtils } from "../../src/core/helpers";

describe("DateUtils", () => {
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("getISODateTime should return local ISO", () => {
        const clock = sinon.useFakeTimers({ now: 1653351166048 });
        const localISO = DateUtils.getISODateTime();
        expect(localISO).to.eq("2022-05-24T02:12:46.048+02:00");

        clock.restore();
    });

    it("getISODateTime should transform JS date and return ISO", () => {
        const ISO = DateUtils.getISODateTime(new Date(1653351166048));
        expect(ISO).to.eq("2022-05-24T02:12:46.048+02:00");
    });
});
